class CfgIcons
{
	class IconBase
	{
		regular = "";
		selected = "";
		render3d = "";
	};
	
	class Base : IconBase
	{
		regular = "sources\MapIcons\Base\map_base_icon_regular.paa";
		selected = "sources\MapIcons\Base\map_base_icon_selected.paa";
		render3d = "sources\MapIcons\Base\map_base_icon_render3d.paa";
	};
	
	class Infil : IconBase
	{
		regular = "sources\MapIcons\Infil\map_infil_icon_regular.paa";
		selected = "sources\MapIcons\Infil\map_infil_icon_selected.paa";
		render3d = "sources\MapIcons\Infil\map_infil_icon_render3d.paa";
	};
	
	class Waypoint : IconBase
	{
		regular = "sources\MapIcons\Waypoint\map_waypoint_icon_regular.paa";
		selected = "sources\MapIcons\Waypoint\map_waypoint_icon_selected.paa";
		render3d = "sources\MapIcons\Waypoint\map_waypoint_icon_render3d.paa";
	};
	
	class NoGenerator : IconBase
	{
		regular = "sources\MapIcons\NoGenerator\map_nogenerator_icon_regular.paa";
		selected = "sources\MapIcons\NoGenerator\map_nogenerator_icon_selected.paa";
		render3d = "sources\MapIcons\NoGenerator\map_nogenerator_icon_render3d.paa";
	};
	
};