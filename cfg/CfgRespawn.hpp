class CfgRoles
{
    class R_CSAT_SpecFor
    {
        displayName = "CSAT Special Forces";
        icon = "\a3\ui_f\data\GUI\Rsc\RscDisplayArsenal\nvgs_ca.paa";
    };
};

class CfgRespawnInventory
{
	class Empty
	{
		displayName = "";
		icon = "";
		role = "";

		uniformClass = "";
		backpack = "";
		weapons[] = {"Throw","Put"};
		magazines[] = {""};
		items[] = {""};
		linkedItems[] = {""};
	};
	
	class L_CSAT_SpecFor_Rifleman: Empty
	{
		role = "R_CSAT_SpecFor";
		
		displayName = "CSAT-SF Rifleman";
		icon = "\a3\ui_f\data\GUI\Cfg\RespawnRoles\assault_ca.paa";
		
		uniformClass = "U_O_CombatUniform_ocamo";
		backpack = "B_Kitbag_rgr";
		
		weapons[] = {"Throw", "Put", "arifle_MX_Black_F", "hgun_Rook40_F", "Rangefinder"};
		magazines[] = {"30Rnd_65x39_caseless_mag", "30Rnd_65x39_caseless_mag", "30Rnd_65x39_caseless_mag", "30Rnd_65x39_caseless_mag", "30Rnd_65x39_caseless_mag", "30Rnd_65x39_caseless_mag", "30Rnd_65x39_caseless_mag_Tracer", "30Rnd_65x39_caseless_mag_Tracer", "16Rnd_9x21_Mag", "16Rnd_9x21_Mag", "16Rnd_9x21_Mag", "16Rnd_9x21_Mag", "16Rnd_9x21_Mag"};
		items[] = {"FirstAidKit", "FirstAidKit", "FirstAidKit", "FirstAidKit", "FirstAidKit"};
		linkedItems[] = {"V_PlateCarrierIAGL_dgtl","H_HelmetIA","G_Balaclava_blk","ItemMap","ItemCompass","ItemWatch","ItemRadio","ItemGPS","NVGoggles_INDEP","optic_MRCO","acc_pointer_IR", "muzzle_snds_h", "muzzle_snds_L"};
	};
};
