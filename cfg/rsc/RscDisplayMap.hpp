class RscDisplayMap
{
	idd = 2063700;
	movingEnable = 0;
	fadein = 0;
	fadeout = 0;
	duration = 999999;
	onLoad = "_this call yvl_fnc__mapDisplayOnLoad";
	onUnload = "_this call yvl_fnc__mapDisplayOnUnload";
	class ControlsBackground
	{
		class Map: RscMapControl
		{
			idc = 1000;
			x = safezoneX;
			y = safezoneY;
			w = safezoneW;
			h = safezoneH;
			colorOutside[] = {0.717,0.796,0.901,1};
			colorGrid[] = {0.1,0.1,0.1,0};
			colorGridMap[] = {0.1,0.1,0.1,0};
			colorLevels[] = {0.286,0.177,0.094,0};
		};
	};
	class Controls
	{
		class Filter: RscPicture
		{
			idc = 999;
			x = safezoneX;
			y = safezoneY;
			w = safezoneW;
			h = safezoneH;
			text = "sources\map_filter_ca.paa";
			colorText[] = {1,1,1,0.75};
		};
	};
};