class CfgFunctions
{
	class BIS
	{
		class init
		{
			class preInit { preInit = 1; file = "preInit.sqf"; };
			class postInit { postInit = 1; file = "postInit.sqf"; };
		};
	};
	
	class yvl
	{
		class General
		{
			file = "scripts";
			
			class onPlayerSpawnClient {};
			class onPlayerSpawnServer {};
			
			class onPreInitClient {};
			class onPreInitServer {};
			
			class onPostInitClient {};
			class onPostInitServer {};
			
			class onMissionStartServer {};
		};
		
		class Carrier
		{
			file = "scripts\Carrier";
			
			class getCarrierCatapultPositions {};
			class spawnCarrierAircraft {};
		};
		
		class AI
		{
			file = "scripts\AI";
			
			class spawnCarrierAircraftAI {};
			class spawnScenarioPreset {};
		};
		
		class Tasks
		{
			file = "scripts\Tasks";
			
			class taskPowerfarmClient {};
			class taskPowerfarmServer {};
		};
		
		class UI_Render3D
		{
			file = "scripts\UI\Render3D";
			
			class __initRender3D { preInit = 1; };
			
			class add3DMarker {};
			class get3DMarker {};
			class remove3DMarker {};
			
			class _draw3DMarkers {};
		};
		
		class UI_MapNG
		{
			file = "scripts\UI\MapNG";
			
			class __initMapNG { preInit = 1; };
			
			class addMapMarker {};
			class getMapMarker {};
			class removeMapMarker {};
			class selectMapMarker {};
			
			class _mapCtrlOnDraw {};
			class _mapCtrlOnMouseMoving {};
			class _mapCtrlOnMouseZChanged {};
			class _mapCtrlOnMouseButtonDown {};
			class _mapDisplayOnLoad {};
			class _mapDisplayOnUnload {};
			class _mapDisplayOnKeyDown {};
		};
	};
};