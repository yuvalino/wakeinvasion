class CfgScenarioPresets
{
	class WreckShipPatrol
	{
		class Group0
		{
			side = "WEST";
			units[] =
			{
				{"B_Soldier_SL_F",{3067.05, 5511.33, 0.00198936}},
				{"B_Soldier_F",{3068.39, 5508.81, 0.00168228}}
			};
			waypoints[] =
			{
				{{3065.45, 5509.43, 0},"HOLD","RED","COMBAT","NO CHANGE","UNCHANGED"}
			};
		};
		class Group1
		{
			side = "WEST";
			units[] =
			{
				{"B_Soldier_F",{2962.78, 5526.57, 0.00268984}},
				{"B_Soldier_F",{2965.57, 5523.27, 0.00268984}}
			};
			waypoints[] =
			{
				{{2963.38, 5526.24, 0},"HOLD","RED","COMBAT","NO CHANGE","UNCHANGED"}
			};
		};
		class Group2
		{
			side = "WEST";
			units[] =
			{
				{"B_Soldier_F",{2997.48, 5529.41, 0.000738144}},
				{"B_medic_F",{2999.92, 5526.7, 0.000745773}}
			};
			waypoints[] =
			{
				{{2995.93, 5527.09, 0},"HOLD","RED","COMBAT","NO CHANGE","UNCHANGED"}
			};
		};
		class Group3
		{
			side = "WEST";
			units[] =
			{
				{"B_soldier_M_F",{2986.16, 5496.41, 0.00123763}},
				{"B_soldier_AR_F",{2988.47, 5494.5, 0.0012387}}
			};
			waypoints[] =
			{
				{{2980.44, 5497.84, 0},"HOLD","RED","COMBAT","NO CHANGE","UNCHANGED"}
			};
		};
		class Group4
		{
			side = "WEST";
			units[] =
			{
				{"B_soldier_AR_F",{2962.67, 5496.12, 0.00143886}},
				{"B_soldier_AAR_F",{2968.12, 5493.51, 0.00138879}}
			};
			waypoints[] =
			{
				{{2961.33, 5503.24, 0},"MOVE","WHITE","AWARE","NO CHANGE","UNCHANGED"},
				{{2974.44, 5512.87, 4.76837e-007},"MOVE","NO CHANGE","UNCHANGED","NO CHANGE","UNCHANGED"},
				{{2983.39, 5537.18, -2.98023e-006},"MOVE","NO CHANGE","UNCHANGED","NO CHANGE","UNCHANGED"},
				{{2998.82, 5540.62, 3.55244e-005},"MOVE","NO CHANGE","UNCHANGED","NO CHANGE","UNCHANGED"},
				{{3010.47, 5524.36, 7.15256e-007},"MOVE","NO CHANGE","UNCHANGED","NO CHANGE","UNCHANGED"},
				{{3005.76, 5494.43, -2.74181e-006},"MOVE","NO CHANGE","UNCHANGED","NO CHANGE","UNCHANGED"},
				{{2982.92, 5490.5, -1.43051e-006},"MOVE","NO CHANGE","UNCHANGED","NO CHANGE","UNCHANGED"},
				{{2963.91, 5501.92, 3.57628e-007},"CYCLE","NO CHANGE","UNCHANGED","NO CHANGE","UNCHANGED"}
			};
		};
	};
	class FuelDepotGuard
	{
		class Group0
		{
			side = "WEST";
			units[] =
			{
				{"B_Soldier_GL_F",{3531.43, 5287.41, 0.00133872}},
				{"B_Soldier_F",{3529.31, 5285.2, 0.00134015}}
			};
			waypoints[] =
			{
				{{3543.65, 5298.7, 0},"MOVE","NO CHANGE","UNCHANGED","COLUMN","UNCHANGED"},
				{{3688.74, 5168.52, -4.76837e-007},"MOVE","NO CHANGE","UNCHANGED","NO CHANGE","UNCHANGED"},
				{{3541.96, 5300.23, 0},"MOVE","NO CHANGE","UNCHANGED","NO CHANGE","UNCHANGED"},
				{{3444.67, 5188.08, 1.43051e-006},"MOVE","NO CHANGE","UNCHANGED","NO CHANGE","UNCHANGED"},
				{{3512.39, 5127.47, 0},"MOVE","NO CHANGE","UNCHANGED","NO CHANGE","UNCHANGED"},
				{{3445.69, 5187.83, -1.66893e-006},"MOVE","NO CHANGE","UNCHANGED","NO CHANGE","UNCHANGED"},
				{{3543.9, 5298.11, 0},"CYCLE","NO CHANGE","UNCHANGED","NO CHANGE","UNCHANGED"}
			};
		};
		class Group1
		{
			side = "WEST";
			units[] =
			{
				{"B_recon_M_F",{3450.71, 5185.2, 0.00143886}},
				{"B_recon_F",{3449.12, 5185.75, 0.00143886}}
			};
			waypoints[] =
			{
				{{3513.52, 5128.7, -7.15256e-007},"MOVE","NO CHANGE","UNCHANGED","NO CHANGE","UNCHANGED"},
				{{3446.42, 5188.11, -2.38419e-007},"MOVE","NO CHANGE","UNCHANGED","NO CHANGE","UNCHANGED"},
				{{3541.37, 5300.66, 8.34465e-006},"MOVE","NO CHANGE","UNCHANGED","NO CHANGE","UNCHANGED"},
				{{3689.73, 5168.47, -2.38419e-007},"MOVE","NO CHANGE","UNCHANGED","NO CHANGE","UNCHANGED"},
				{{3541.06, 5301.23, 2.38419e-007},"MOVE","NO CHANGE","UNCHANGED","NO CHANGE","UNCHANGED"},
				{{3445.01, 5188.26, -1.43051e-006},"MOVE","NO CHANGE","UNCHANGED","NO CHANGE","UNCHANGED"},
				{{3514.11, 5128.82, 1.66893e-006},"CYCLE","NO CHANGE","UNCHANGED","NO CHANGE","UNCHANGED"}
			};
		};
		class Group2
		{
			side = "WEST";
			units[] =
			{
				{"B_Soldier_TL_F",{3438.31, 5159.42, 0.0383654}},
				{"B_soldier_AR_F",{3436.78, 5155.92, 0.0383654}},
				{"B_Soldier_GL_F",{3437.08, 5152.72, 0.0383654}},
				{"B_soldier_LAT_F",{3439.07, 5154.84, 0.0383654}}
			};
			waypoints[] =
			{
				{{3435.04, 5156.71, 0},"HOLD","RED","STEALTH","NO CHANGE","LIMITED"}
			};
		};
		class Group3
		{
			side = "WEST";
			units[] =
			{
				{"B_recon_M_F",{3565.4, 5175.37, 0.00143886}},
				{"B_recon_F",{3570.4, 5170.37, 0.00143886}}
			};
			waypoints[] =
			{
				{{3563.73, 5159.32, 0},"HOLD","RED","COMBAT","NO CHANGE","UNCHANGED"}
			};
		};
		class Group4
		{
			side = "WEST";
			units[] =
			{
				{"B_sniper_F",{3594.74, 5140.05, 0.00143886}},
				{"B_spotter_F",{3599.74, 5135.05, 0.00178909}}
			};
			waypoints[] =
			{
				{{3588.81, 5135.32, 0},"HOLD","RED","STEALTH","NO CHANGE","UNCHANGED"}
			};
		};
		class Group5
		{
			side = "WEST";
			units[] =
			{
				{"B_recon_TL_F",{3699.32, 5018.97, 0.00168753}},
				{"B_recon_M_F",{3701.48, 5015.07, 0.00168753}},
				{"B_recon_medic_F",{3694.32, 5013.97, 0.00198603}},
				{"B_recon_LAT_F",{3696.33, 5015.5, 0.00168753}},
				{"B_recon_JTAC_F",{3697.38, 5011.95, 0.00198603}},
				{"B_recon_exp_F",{3700.44, 5012.36, 0.00168753}}
			};
			waypoints[] =
			{
				{{3691.47, 5010.06, 0},"HOLD","RED","COMBAT","NO CHANGE","UNCHANGED"}
			};
		};
		class Group6
		{
			side = "WEST";
			units[] =
			{
				{"B_sniper_F",{3677.9, 5024.16, 15.2738}}
			};
			waypoints[] =
			{
			};
		};
		class Group7
		{
			side = "WEST";
			units[] =
			{
				{"B_Soldier_TL_F",{3643.43, 4996.39, 0.00143886}},
				{"B_soldier_AR_F",{3645.29, 4996.04, 0.00143886}},
				{"B_Soldier_GL_F",{3647.38, 4996.4, 0.00143886}},
				{"B_soldier_LAT_F",{3645.1, 4998.9, 0.00143886}}
			};
			waypoints[] =
			{
				{{3632.6, 4992.07, 0},"MOVE","NO CHANGE","UNCHANGED","NO CHANGE","UNCHANGED"},
				{{3605.66, 4960.74, 0},"MOVE","NO CHANGE","UNCHANGED","NO CHANGE","UNCHANGED"},
				{{3499.55, 5054.14, 0},"MOVE","NO CHANGE","UNCHANGED","NO CHANGE","UNCHANGED"},
				{{3513.99, 5091.37, 0},"MOVE","NO CHANGE","UNCHANGED","NO CHANGE","UNCHANGED"},
				{{3501.47, 5052.77, 0},"MOVE","NO CHANGE","UNCHANGED","NO CHANGE","UNCHANGED"},
				{{3607.75, 4959.2, 0},"MOVE","NO CHANGE","UNCHANGED","NO CHANGE","UNCHANGED"},
				{{3636.42, 4990.37, 0},"CYCLE","NO CHANGE","UNCHANGED","NO CHANGE","UNCHANGED"}
			};
		};
		class Group8
		{
			side = "WEST";
			units[] =
			{
				{"B_soldier_AR_F",{3741.12, 5022.33, 1.15371}},
				{"B_medic_F",{3741.84, 5021.54, 1.06718}},
				{"B_Soldier_GL_F",{3742.76, 5022.34, 1.06718}},
				{"B_soldier_AAR_F",{3742.12, 5023.08, 1.06718}},
				{"B_soldier_M_F",{3743.29, 5021.7, 1.28517}}
			};
			waypoints[] =
			{
				{{3737.55, 5027.81, 0},"MOVE","NO CHANGE","UNCHANGED","NO CHANGE","UNCHANGED"},
				{{3602.58, 5150.28, 0},"MOVE","NO CHANGE","UNCHANGED","NO CHANGE","UNCHANGED"},
				{{3557.81, 5136.49, 0},"MOVE","NO CHANGE","UNCHANGED","NO CHANGE","UNCHANGED"},
				{{3538.22, 5115.43, -1.19209e-006},"MOVE","NO CHANGE","UNCHANGED","NO CHANGE","UNCHANGED"},
				{{3503.4, 5125.12, 0},"MOVE","NO CHANGE","UNCHANGED","NO CHANGE","UNCHANGED"},
				{{3449.57, 5169.78, 0},"MOVE","NO CHANGE","UNCHANGED","NO CHANGE","UNCHANGED"},
				{{3438.34, 5229.03, 7.15256e-007},"MOVE","NO CHANGE","UNCHANGED","NO CHANGE","UNCHANGED"},
				{{3499.02, 5297.12, 7.15256e-007},"MOVE","NO CHANGE","UNCHANGED","NO CHANGE","UNCHANGED"},
				{{3557.08, 5303.71, 2.38419e-007},"MOVE","NO CHANGE","UNCHANGED","NO CHANGE","UNCHANGED"},
				{{3801.99, 5086.12, 0},"MOVE","NO CHANGE","UNCHANGED","NO CHANGE","UNCHANGED"},
				{{3764.06, 5020.75, 2.38419e-007},"MOVE","NO CHANGE","UNCHANGED","NO CHANGE","UNCHANGED"},
				{{3739.21, 5027.55, 0},"CYCLE","NO CHANGE","UNCHANGED","NO CHANGE","UNCHANGED"}
			};
			vehicle[] = {"B_LSV_01_unarmed_F", {3741.77, 5022.5, 0.000141621}, 318.996};
		};
		class Group9
		{
			side = "WEST";
			units[] =
			{
				{"B_recon_TL_F",{3788.98, 5011.36, 0.00133896}},
				{"B_recon_M_F",{3791.06, 5006.53, 0.00143909}},
				{"B_recon_medic_F",{3786.71, 5007.71, 0.00143909}},
				{"B_recon_F",{3789.28, 5005.37, 0.00143933}}
			};
			waypoints[] =
			{
				{{3788.62, 5009.48, 0},"HOLD","RED","COMBAT","NO CHANGE","UNCHANGED"}
			};
		};
	};
	class PierStaff
	{
		class Group0
		{
			side = "WEST";
			units[] =
			{
				{"B_Helipilot_F",{4232.79, 4582.62, 1.1902}},
				{"B_Helipilot_F",{4232.38, 4583.18, 1.19367}},
				{"B_Soldier_F",{4232.15, 4583.75, 0.949157}},
				{"B_Soldier_F",{4234.04, 4582.97, 0.945378}},
				{"B_Soldier_F",{4233.39, 4582.35, 0.940448}},
				{"B_Soldier_F",{4232.82, 4584.34, 0.943155}}
			};
			waypoints[] =
			{
				{{4106.35, 4677.84, 0},"LOITER","NO CHANGE","UNCHANGED","NO CHANGE","UNCHANGED"}
			};
			vehicle[] = {"B_Heli_Light_01_F", {4233.49, 4583.69, 0}, 228.447};
		};
		class Group1
		{
			side = "WEST";
			units[] =
			{
				{"B_Soldier_F",{4230.93, 4887.18, 14.327}},
				{"B_Soldier_F",{4232.12, 4886.92, 14.3278}},
				{"B_Soldier_F",{4235.51, 4886.31, 12.6499}}
			};
			waypoints[] =
			{
			};
			vehicle[] = {"B_Boat_Armed_01_minigun_F", {4231.63, 4886.94, 12.8601}, 281.512};
		};
		class Group2
		{
			side = "WEST";
			units[] =
			{
				{"B_Soldier_TL_F",{4115.31, 4597.22, 0.00143886}},
				{"B_soldier_AR_F",{4116.58, 4594.69, 0.00143886}},
				{"B_Soldier_GL_F",{4112.82, 4594.59, 0.00143886}},
				{"B_soldier_LAT_F",{4119.72, 4593.8, 0.00143886}}
			};
			waypoints[] =
			{
				{{4114.39, 4594.64, 3.72},"HOLD","RED","COMBAT","NO CHANGE","UNCHANGED"}
			};
		};
		class Group3
		{
			side = "WEST";
			units[] =
			{
				{"B_Soldier_TL_F",{4055.03, 4751.79, 0.0192523}},
				{"B_soldier_AR_F",{4056.3, 4749.27, 0.0170503}},
				{"B_Soldier_GL_F",{4052.55, 4749.16, 0.0170298}},
				{"B_soldier_LAT_F",{4059.45, 4748.37, 0.021282}}
			};
			waypoints[] =
			{
				{{4054.11, 4749.22, 3.4997},"HOLD","RED","COMBAT","NO CHANGE","UNCHANGED"}
			};
		};
		class Group4
		{
			side = "WEST";
			units[] =
			{
				{"B_Soldier_TL_F",{4039.31, 4665.86, 0.251932}},
				{"B_soldier_AR_F",{4040.58, 4663.33, 0.229628}},
				{"B_Soldier_GL_F",{4036.82, 4663.23, 0.291775}},
				{"B_soldier_LAT_F",{4043.72, 4662.44, 0.597396}}
			};
			waypoints[] =
			{
				{{4038.39, 4663.29, 7.21161},"HOLD","RED","COMBAT","NO CHANGE","UNCHANGED"}
			};
		};
		class Group5
		{
			side = "WEST";
			units[] =
			{
				{"B_Soldier_TL_F",{4053.23, 4789.49, 0.00148869}},
				{"B_soldier_AR_F",{4054.5, 4786.96, 0.00148916}},
				{"B_Soldier_GL_F",{4050.74, 4786.86, 0.00467634}},
				{"B_soldier_LAT_F",{4057.64, 4786.07, 0.00572133}}
			};
			waypoints[] =
			{
				{{4052.31, 4786.91, 3.48169},"HOLD","RED","COMBAT","NO CHANGE","UNCHANGED"}
			};
		};
		class Group6
		{
			side = "WEST";
			units[] =
			{
				{"B_Soldier_GL_F",{4221.25, 4709.57, 0.00203586}},
				{"B_Soldier_F",{4222.6, 4706.16, 0.00213528}}
			};
			waypoints[] =
			{
				{{4218.23, 4713.15, -3.8147e-006},"MOVE","NO CHANGE","UNCHANGED","NO CHANGE","UNCHANGED"},
				{{4132.03, 4809, 0},"MOVE","NO CHANGE","UNCHANGED","NO CHANGE","UNCHANGED"},
				{{4218.83, 4714.24, -2.86102e-006},"CYCLE","NO CHANGE","UNCHANGED","NO CHANGE","UNCHANGED"}
			};
		};
		class Group7
		{
			side = "WEST";
			units[] =
			{
				{"B_Soldier_GL_F",{4025.21, 4785.17, 0.00153899}},
				{"B_Soldier_F",{4026.56, 4781.76, 0.00153899}}
			};
			waypoints[] =
			{
				{{4029.87, 4786.14, 2.38419e-007},"MOVE","NO CHANGE","UNCHANGED","NO CHANGE","UNCHANGED"},
				{{4079.67, 4810.49, 2.38419e-007},"MOVE","NO CHANGE","UNCHANGED","NO CHANGE","UNCHANGED"},
				{{4092.02, 4741.64, 3.09944e-006},"MOVE","NO CHANGE","UNCHANGED","NO CHANGE","UNCHANGED"},
				{{4078.13, 4811.33, 0},"MOVE","NO CHANGE","UNCHANGED","NO CHANGE","UNCHANGED"},
				{{4029.92, 4787.23, -4.76837e-007},"CYCLE","NO CHANGE","UNCHANGED","NO CHANGE","UNCHANGED"}
			};
		};
		class Group8
		{
			side = "WEST";
			units[] =
			{
				{"B_Soldier_GL_F",{4108.67, 4836.52, -0.00126159}},
				{"B_Soldier_F",{4110.02, 4833.11, 0.000237703}}
			};
			waypoints[] =
			{
				{{4106.27, 4831.26, 0},"HOLD","RED","COMBAT","NO CHANGE","UNCHANGED"}
			};
		};
		class Group9
		{
			side = "WEST";
			units[] =
			{
				{"B_Soldier_GL_F",{4155.56, 4580.87, -0.000811338}},
				{"B_Soldier_F",{4156.91, 4577.46, -0.0005126}}
			};
			waypoints[] =
			{
				{{4153.16, 4575.61, 0},"HOLD","RED","COMBAT","NO CHANGE","UNCHANGED"}
			};
		};
		class Group10
		{
			side = "WEST";
			units[] =
			{
				{"B_Soldier_GL_F",{4123.27, 4644.23, -0.00156188}},
				{"B_Soldier_F",{4124.62, 4640.82, -6.24657e-005}}
			};
			waypoints[] =
			{
				{{4120.88, 4638.97, 0},"HOLD","RED","COMBAT","NO CHANGE","UNCHANGED"}
			};
		};
		class Group11
		{
			side = "WEST";
			units[] =
			{
				{"B_Soldier_GL_F",{4071.48, 4711.86, -0.00331497}},
				{"B_Soldier_F",{4072.83, 4708.45, 0.00118732}}
			};
			waypoints[] =
			{
				{{4069.08, 4706.6, 0.0010004},"HOLD","RED","COMBAT","NO CHANGE","UNCHANGED"}
			};
		};
		class Group12
		{
			side = "WEST";
			units[] =
			{
				{"B_Soldier_GL_F",{3957.78, 4746.16, -0.00156188}},
				{"B_Soldier_F",{3959.13, 4742.75, 0.247223}}
			};
			waypoints[] =
			{
				{{3955.38, 4740.9, 0.255504},"HOLD","RED","COMBAT","NO CHANGE","UNCHANGED"}
			};
		};
		class Group13
		{
			side = "WEST";
			units[] =
			{
				{"B_Soldier_TL_F",{4010.51, 4751.11, 0.000593185}},
				{"B_soldier_AR_F",{4007.87, 4752.14, 0.000640631}},
				{"B_Soldier_GL_F",{4010.59, 4754.73, 0.000593185}},
				{"B_soldier_LAT_F",{4008.09, 4754.75, 0.000593185}}
			};
			waypoints[] =
			{
				{{4012.72, 4747.73, -2.14577e-006},"MOVE","NO CHANGE","UNCHANGED","NO CHANGE","UNCHANGED"},
				{{4108.62, 4634.08, 0},"MOVE","NO CHANGE","UNCHANGED","NO CHANGE","UNCHANGED"},
				{{4011.69, 4746.35, -5.00679e-006},"CYCLE","NO CHANGE","UNCHANGED","NO CHANGE","UNCHANGED"}
			};
		};
		class Group14
		{
			side = "WEST";
			units[] =
			{
				{"B_Soldier_GL_F",{4144.21, 4609.65, -0.00156188}},
				{"B_Soldier_F",{4140.92, 4611.27, -6.24657e-005}}
			};
			waypoints[] =
			{
				{{4149, 4603.67, 0},"MOVE","NO CHANGE","UNCHANGED","NO CHANGE","UNCHANGED"},
				{{4181.61, 4567.98, -4.76837e-007},"LOITER","NO CHANGE","UNCHANGED","NO CHANGE","UNCHANGED"},
				{{4242.83, 4617.86, 0},"LOITER","NO CHANGE","UNCHANGED","NO CHANGE","UNCHANGED"},
				{{4181.91, 4566.23, 0},"LOITER","NO CHANGE","UNCHANGED","NO CHANGE","UNCHANGED"},
				{{4151.16, 4603.44, 0},"CYCLE","NO CHANGE","UNCHANGED","NO CHANGE","UNCHANGED"}
			};
		};
	};
	class PowerFarm
	{
		class Group0
		{
			side = "WEST";
			units[] =
			{
				{"B_recon_M_F",{2611.52, 6279.13, 0.00213957}},
				{"B_recon_F",{2613.88, 6277.62, 0.00213051}}
			};
			waypoints[] =
			{
				{{2612.84, 6281.92, 0},"MOVE","NO CHANGE","UNCHANGED","COLUMN","UNCHANGED"},
				{{2595.55, 6320.72, 0},"MOVE","NO CHANGE","UNCHANGED","NO CHANGE","UNCHANGED"},
				{{2558.18, 6347.1, 7.62939e-006},"MOVE","NO CHANGE","UNCHANGED","NO CHANGE","UNCHANGED"},
				{{2515.18, 6364.9, 0},"MOVE","NO CHANGE","UNCHANGED","NO CHANGE","UNCHANGED"},
				{{2477.19, 6367.18, -5.62668e-005},"MOVE","NO CHANGE","UNCHANGED","NO CHANGE","UNCHANGED"},
				{{2439.83, 6354.2, 5.14984e-005},"MOVE","NO CHANGE","UNCHANGED","NO CHANGE","UNCHANGED"},
				{{2411.7, 6329.9, 3.71933e-005},"MOVE","NO CHANGE","UNCHANGED","NO CHANGE","UNCHANGED"},
				{{2394.81, 6302.11, -5.72205e-006},"MOVE","NO CHANGE","UNCHANGED","NO CHANGE","UNCHANGED"},
				{{2413.52, 6328.44, 5.14984e-005},"MOVE","NO CHANGE","UNCHANGED","NO CHANGE","UNCHANGED"},
				{{2440.24, 6351.6, 3.43323e-005},"MOVE","NO CHANGE","UNCHANGED","NO CHANGE","UNCHANGED"},
				{{2477.24, 6364.59, -1.23978e-005},"MOVE","NO CHANGE","UNCHANGED","NO CHANGE","UNCHANGED"},
				{{2515.4, 6362.65, 0},"MOVE","NO CHANGE","UNCHANGED","NO CHANGE","UNCHANGED"},
				{{2557.37, 6344.94, -1.90735e-005},"MOVE","NO CHANGE","UNCHANGED","NO CHANGE","UNCHANGED"},
				{{2593.82, 6317.77, -4.76837e-007},"MOVE","NO CHANGE","UNCHANGED","NO CHANGE","UNCHANGED"},
				{{2610.15, 6281, 0},"CYCLE","NO CHANGE","UNCHANGED","NO CHANGE","UNCHANGED"}
			};
		};
		class Group1
		{
			side = "WEST";
			units[] =
			{
				{"B_Soldier_GL_F",{2466.75, 6129.53, -0.00177479}},
				{"B_Soldier_F",{2464.01, 6129.4, -0.00181484}}
			};
			waypoints[] =
			{
				{{2470.42, 6128.02, 0},"MOVE","NO CHANGE","UNCHANGED","COLUMN","UNCHANGED"},
				{{2511.42, 6124.38, 0},"MOVE","NO CHANGE","UNCHANGED","NO CHANGE","UNCHANGED"},
				{{2544.16, 6130.43, 3.8147e-005},"MOVE","NO CHANGE","UNCHANGED","NO CHANGE","UNCHANGED"},
				{{2571.5, 6142.74, -8.58307e-006},"MOVE","NO CHANGE","UNCHANGED","NO CHANGE","UNCHANGED"},
				{{2602.75, 6173.38, -1.52588e-005},"MOVE","NO CHANGE","UNCHANGED","NO CHANGE","UNCHANGED"},
				{{2573.97, 6141.61, 2.38419e-005},"MOVE","NO CHANGE","UNCHANGED","NO CHANGE","UNCHANGED"},
				{{2545.95, 6128.64, 2.86102e-006},"MOVE","NO CHANGE","UNCHANGED","NO CHANGE","UNCHANGED"},
				{{2510.71, 6121.84, 8.58307e-006},"MOVE","NO CHANGE","UNCHANGED","NO CHANGE","UNCHANGED"},
				{{2470.09, 6126.02, 0},"CYCLE","NO CHANGE","UNCHANGED","NO CHANGE","UNCHANGED"}
			};
		};
		class Group2
		{
			side = "WEST";
			units[] =
			{
				{"B_Soldier_TL_F",{2467.6, 6243.84, 0.00198746}},
				{"B_soldier_AR_F",{2478.53, 6238.03, 0.00209045}},
				{"B_Soldier_GL_F",{2461.43, 6235.24, 0.00208092}},
				{"B_soldier_LAT_F",{2492.3, 6239.71, 0.00173187}}
			};
			waypoints[] =
			{
				{{2470.15, 6236.45, 0},"HOLD","RED","COMBAT","NO CHANGE","UNCHANGED"}
			};
		};
	};
};