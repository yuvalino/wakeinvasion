/* Filename: 	fn_spawnScenarioPreset.sqf
 * Author: 		yuvalino
 * Description: Spawn groups of enemies from CfgDynamicSpawns
 *
 *
 * [presetClass] call yvl_fnc_spawnScenarioPreset
 *
 * 0 - presetClass (string): CfgScenarioPresets class to spawn
 *
 * No return
 *
 */
params ["_dynClass"];

private _sideKey = ["EAST", "WEST", "GUER", "CIV", "UNKNOWN", "ENEMY", "FRIENDLY", "LOGIC", "EMPTY", "AMBIENT LIFE"];
private _sideVal = [east, west, resistance, civilian, sideUnknown, sideEnemy, sideFriendly, sideLogic, sideEmpty, sideAmbientLife];

{
	private _this_grp_cfg = _x;
	private _side_idx = _sideKey find ((_this_grp_cfg >> "side") call BIS_fnc_getCfgData);
	private _units_arr = (_this_grp_cfg >> "units") call BIS_fnc_GetCfgData;
	private _wp_arr = (_this_grp_cfg >> "waypoints") call BIS_fnc_GetCfgData;
	private _veh_arr = (_this_grp_cfg >> "vehicle") call BIS_fnc_GetCfgData;
	
	if (count _units_arr > 0 && _side_idx != -1) then {
		private _grp = createGroup (_sideVal select _side_idx);
		private _veh = nil;
		
		if ( !isNil "_veh_arr" ) then {
			_veh_arr params ["_vehClass", "_vehPosition", "_vehDir"];
			_veh = createVehicle[_vehClass, _vehPosition, [], 0, "CAN_COLLIDE"];
			_veh setDir _vehDir; 
			_veh lock 3;
		};
		
		{
			_x params ["_unitClass", "_unitPosition"];
			private _curr_unit = _grp createUnit [_unitClass, _unitPosition, [], 0, "CAN_COLLIDE"];
			
			if ( !isNil "_veh" ) then {
				_curr_unit moveInAny _veh;
			};
		} foreach (_units_arr);
		
		{
			_x params ["_position", "_type", "_combatMode", "_behaviour", "_formation", "_speedMode"];
			private _wp = _grp addWaypoint [_position, 0];
			_wp setWaypointType _type;
			_wp setWaypointCombatMode _combatMode;
			_wp setWaypointBehaviour _behaviour;
			_wp setWaypointFormation _formation;
			_wp setWaypointSpeed _speedMode;
		} foreach (_wp_arr);
		
	};
} foreach ("true" configClasses (missionConfigFile >> "CfgScenarioPresets" >> _dynClass));