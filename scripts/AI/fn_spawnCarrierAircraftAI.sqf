/* Filename: 	fn_spawnCarrierAircraftAI.sqf
 * Author: 		yuvalino
 * Description: Spawn planes on carrier and catapult launch
 *
 *
 * [carrier, aircraftClass, pilotClass, pilotGroup] call yvl_fnc_spawnCarrierAircraftAI
 *
 * 0 - carrier (object): 		Aircraft carrier to spawn aircraft on
 * 1 - aircraftClass (string): 	Aircraft vehicle class to spawn
 * 2 - pilotClass (string): 	Pilot unit model class
 * 3 - pilotGroup (group): 		Group to spawn pilot into
 *
 * Returns newly created aircraft or nil if failed
 *
 */

params["_carrier", "_aircraftClass", "_pilotClass", "_group"];

private _aircraftSpawnInfo = [_carrier, _aircraftClass] call yvl_fnc_spawnAircraftAircraft;
private _aircraft = _aircraftSpawnInfo select 0;
private _catapultAnimations = _aircraftSpawnInfo select 1;

if ( _aircraft == nil ) exitWith { nil };

private _pilot = _group createUnit [_pilotClass, [0,0,0], [], 0, "NONE"];

[_catapultAnimations, _aircraft, _pilot] spawn {

	params ["_catapultAnimations", "_aircraft", "_pilot"];

	_catapultAnimations set [2, 10];
	_catapultAnimations spawn BIS_fnc_Carrier01AnimateDeflectors;
	_aircraft engineOn true;
	
	sleep 14;
	_pilot moveInDriver _aircraft;
	[_aircraft] spawn BIS_fnc_AircraftCatapultLaunch;
	
	sleep 2;
	_catapultAnimations set [2, 0];
	_catapultAnimations spawn BIS_fnc_Carrier01AnimateDeflectors;

};

_aircraft;