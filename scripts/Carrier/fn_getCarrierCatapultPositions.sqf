/* Filename: 	fn_getCarrierCatapultPositions.sqf
 * Author: 		yuvalino
 * Description: Get positions of catapults on an aircraft carrier
 *
 *
 * [carrier] call yvl_fnc_getCarrierCatapultPositions
 *
 * 0 - carrier (object): Aircraft Carrier object in the world
 *
 * Returns array of catapult data array
 * Catapult data array: [worldPosition, worldDirection, [carrierPart, AnimationData]]
 *
 */
params["_carrier"];

private _carrierDir = getDir _carrier;
private _carrierParts = _carrier getVariable ["bis_carrierParts", []];
private _catapultPositions = [];

{
	private _carrierPart = _x select 0;
	private _partCfg = configFile >> "CfgVehicles" >> typeOf(_carrierPart) >> "Catapults";
	
	if ( isClass _partCfg ) then
	{
		{
			private _pos = _carrierPart modelToWorldWorld ( _carrierPart selectionPosition ( getText (_x >> "memoryPoint") ) );
			private _dir = (180 + _carrierDir - getNumber (_x >> "dirOffset"));
			private _anims = getArray ( _x >> "animations");
			
			_catapultPositions pushback [_pos, _dir, [_carrierPart, _anims]];
		} foreach ( "true" configClasses _partCfg );
	};
	
} foreach (_carrierParts);

_catapultPositions;