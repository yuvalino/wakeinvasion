/* Filename: 	fn_spawnCarrierAircraft.sqf
 * Author: 		yuvalino
 * Description: Spawn an aircraft on an aircraft carrier's random catapult position
 *
 *
 * [carrier, aircraftClass] call yvl_fnc_spawnCarrierAircraft
 *
 * 0 - carrier (object): 		Aircraft Carrier object in the world
 * 1 - aircraftClass (string): 	Aircraft class to spawn on the aircraft carrier
 *
 * Returns array of [spawnedAircraft, catapultAnimationData]
 * 		   or nil if no catapult position found on the aircraft
 */
params["_carrier", "_aircraftClass"];

private _spawnInfos = [];

{
	if ( count ( nearestObjects [_x select 0, [], 5] ) == 0) then {
		_spawnInfos pushback _x;
	};
} foreach ( [_carrier] call yvl_fnc_getCarrierCatapultPositions );

if ( ( count _spawnInfos ) == 0) exitWith { nil };

private _spawnInfo = selectRandom _spawnInfos;

private _spawnPosition = _spawnInfo select 0;
private _spawnDir = _spawnInfo select 1;
private _catapultAnimInfo = _spawnInfo select 2;

private _aircraft = _aircraftClass createVehicle [0, 0, 0];

private _spawnPosition set [2, ( _spawnPosition select 2) - ( ( ( boundingBox _aircraft ) select 0 ) select 2 )];

private _aircraft setPosWorld _spawnPosition;
private _aircraft setDir _spawnDir;

[_aircraft, _catapultAnimInfo];
