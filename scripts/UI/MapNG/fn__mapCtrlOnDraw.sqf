/* Filename: 	fn__mapCtrlOnDraw.sqf
 * Author: 		yuvalino
 * Description: On RscDisplayMap's MapControl Draw event handler
 */
params ["_ctrlMap"];

{
	_x params [ "_id", "_target", "_text", "_color", "_icon", "_icon_full" ];
	
	private _mul = 1;
	
	if (!( _target isEqualType [])) then
	{
		_target = _target modelToWorldVisual [0,0,0];
	};
	
	if ( Map_HoverItem == _id || Map_SelectedItem == _id ) then 
	{
		_color set [3, 1];
	} 
	else 
	{
		_color set [3, 0.67];
	};
	
	private _size = intrn__map_size;
	
	if ( Map_SelectedItem == _id ) then 
	{
		_icon = _icon_full;
	};
	
	if ( Map_HoverItem == _id ) then 
	{
		_icon = _icon_full;
		_mul = intrn__map_hovermul;
	};
	
	if ( Map_HoverItem != _id && Map_SelectedItem != _id ) then
	{
		_text = "";
	};
	
	_ctrlMap drawIcon [ _icon, _color, _target, _size * _mul, _size * _mul, 0, _text, 2, intrn__map_fontsize * _mul, "TahomaB", "Left" ];

} foreach ( Map_Items );