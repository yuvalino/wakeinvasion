/* Filename: 	fn_addMapMarker.sqf
 * Author: 		yuvalino
 * Description: Add a new map marker
 *
 *
 * [target, text, color, iconClass] call yvl_fnc_addMapMarker
 *
 * 0 - target (position / object): 	Map marker position / object target (will follow moving objects)
 * 1 - text (string): 				Map marker text
 * 2 - color (array): 				Array of [R,G,B,A] colors (0 to 1 range)
 * 3 - iconClass (string): 			Icon class CfgIcons
 *
 * Returns integer map marker ID or -1 on error
 *
 */

params ["_target", "_text", "_color", "_iconClass"];

// Get iconClass config data
private _cfg = (missionConfigFile >> "CfgIcons" >> _iconClass);
if (
	!(isClass _cfg) ||
	!(isText (_cfg >> "regular")) ||
	!(isText (_cfg >> "selected"))
) exitWith { -1 };

private _icon_regular = (_cfg >> "regular") call BIS_fnc_getCfgData;
private _icon_selected = (_cfg >> "selected") call BIS_fnc_getCfgData;

// Acquire new item ID
private _id = intrn__map_nxtidx;
intrn__map_nxtidx = intrn__map_nxtidx + 1;

// Add new item to the map item list
Map_Items pushback [
	_id,
	_target,
	_text,
	_color,
	format [ "%1%2", MISSION_ROOT, _icon_regular ],
	format [ "%1%2", MISSION_ROOT, _icon_selected ],
	_iconClass
	
];

// Return the newly added item's ID
_id;
