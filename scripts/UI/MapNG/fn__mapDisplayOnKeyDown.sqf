/* Filename: 	fn__mapDisplayOnKeyDown.sqf
 * Author: 		yuvalino
 * Description: On RscDisplayMap KeyDown event handler
 */
params ["_display", "_keyCode", "_stateShift", "_stateCtrl", "_stateAlt"];

private _ret = false;
if( _keyCode == 0x01 ) then {
	closeDialog 1;
	_ret = true;
};

_ret