/* Filename: 	fn_selectMapMarker.sqf
 * Author: 		yuvalino
 * Description: Selects a map marker (shows it in 3D)
 *
 *
 * [MapMarkerID] call yvl_fnc_selectMapMarker
 *
 * 0 - MapMarkerID (int): Map marker ID
 *
 * No return
 *
 */

params ["_id"];

if ( _id == -1 ) then 
{

	if ( Map_SelectedItemRender3D != -1 ) then 
	{
		[Map_SelectedItemRender3D] call yvl_fnc_remove3DMarker;
	};

	Map_SelectedItem = -1;
	Map_SelectedItemRender3D = -1;
}
else
{
	([_id] call yvl_fnc_getMapMarker) params ["_target", "_text", "_color", "_icon_regular", "_icon_selected", "_iconClass"];
	
	if ( Map_SelectedItemRender3D != -1 ) then 
	{
		[Map_SelectedItemRender3D] call yvl_fnc_remove3DMarker;
	};
	
	Map_SelectedItemRender3D = [_target, _color, _iconClass] call yvl_fnc_add3dMarker;
	Map_SelectedItem = _id;
};