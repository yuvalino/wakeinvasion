/* Filename: 	fn_getMapMarker.sqf
 * Author: 		yuvalino
 * Description: Gets map marker data
 *
 *
 * [MapMarkerID] call yvl_fnc_getMapMarker
 *
 * 0 - MapMarkerID (int): Map marker ID
 *
 * Returns map marker data of marker or [] on error
 * Map marker data: [target, text, color, regular_icon_path, selected_icon_path, iconClass]
 *
 */

params ["_id"];

private _data = [];
private _idx = Map_Items findIf { _x select 0 == _id };
if ( _idx != -1 ) then {
	_data = +(Map_Items select _idx);
	_data deleteAt 0;
};

_data;