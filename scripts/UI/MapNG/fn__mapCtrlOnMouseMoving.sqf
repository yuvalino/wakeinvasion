/* Filename: 	fn__mapCtrlOnMouseMoving.sqf
 * Author: 		yuvalino
 * Description: On RscDisplayMap's MapControl MouseMoving event handler
 */
params ["_ctrlMap", "_mousePositionX", "_mousePositionY", "_mouseOver"];

private _hover_id = -1;
private _min_dist = -1;
private _mousePosition = [ _mousePositionX, _mousePositionY ];

{
	_x params [ "_id", "_target", "_text", "_color", "_icon", "_icon_full" ];
	
	if (!( _target isEqualType [])) then
	{
		_target = _target modelToWorldVisual [0,0,0];
	};
	
	_target = _ctrlMap ctrlMapWorldToScreen _target;
	private _curr_dist = _target distance _mousePosition;
	
	if ( _min_dist == -1 ) then
	{
		if ( _curr_dist < 0.015 ) then 
		{
			_hover_id = _id;
			_min_dist = _curr_dist;
		};
	}
	else
	{
		if ( _curr_dist < _min_dist ) then 
		{
			_hover_id = _id;
			_min_dist = _curr_dist;
		};
	};
}foreach Map_Items;

Map_HoverItem = _hover_id;

true;