/* Filename: 	fn__mapDisplayOnUnload.sqf
 * Author: 		yuvalino
 * Description: On RscDisplayMap Unload event handler
 */
params ["_display", "_exitCode"];

private _id = -1;

_id = _display getVariable "eh_onKeyDown";
_display displayRemoveEventHandler ["KeyDown", _id];
_display setVariable ["eh_onKeyDown", nil];

private _ctrlMap = _display displayCtrl 1000;

_id = _ctrlMap getVariable "eh_onDraw";
_ctrlMap ctrlRemoveEventHandler ["Draw", _id];
_ctrlMap setVariable ["eh_onDraw", nil];

_id = _ctrlMap getVariable "eh_onMouseMoving";
_ctrlMap ctrlRemoveEventHandler ["MouseMoving", _id];
_ctrlMap setVariable ["eh_onMouseMoving", nil];

_id = _ctrlMap getVariable "eh_onMouseButtonDown";
_ctrlMap ctrlRemoveEventHandler ["MouseButtonDown", _id];
_ctrlMap setVariable ["eh_onMouseButtonDown", nil];

_id = _ctrlMap getVariable "eh_onMouseZChanged";
_ctrlMap ctrlRemoveEventHandler ["MouseZChanged", _id];
_ctrlMap setVariable ["eh_onMouseZChanged", nil];