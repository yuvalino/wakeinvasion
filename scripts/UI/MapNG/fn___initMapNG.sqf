/* Filename: 	_initMapNG.sqf
 * Author: 		yuvalino
 * Description: Map New Generation initialization script
 */

if ( !hasInterface ) exitWith {};
 
intrn__map_nxtidx = 0;
intrn__map_size = 24 / (getResolution select 5);
intrn__map_fontsize = 0.06;
intrn__map_hovermul = 1.2;

Map_Items = [];
Map_HoverItem = -1;
Map_SelectedItem = -1;
Map_SelectedItemRender3D = -1;

Map_WaypointItemID = -1;

Map_Waypoint_Color = [0.9, 0.9, 0.9, 1];
Map_Waypoint_Text = "Waypoint";
Map_Waypoint_IconClass = "Waypoint";

addMissionEventHandler ["Map",{ 
	params["_mapOpened", "_forced"];
	
	if ( _mapOpened ) then {
		[] spawn {
			waitUntil { visibleMap };
			if (missionNamespace getVariable ["BIS_RscRespawnControlsMap_shown", false]) exitWith {};
			openMap false;
			
			if ( !dialog ) then {
				createDialog "RscDisplayMap";
			};
		};
		
		true;
	};
}];