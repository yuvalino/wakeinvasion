/* Filename: 	fn_removeMapMarker.sqf
 * Author: 		yuvalino
 * Description: Removes a map marker
 *
 *
 * [MapMarkerID] call yvl_fnc_removeMapMarker
 *
 * 0 - MapMarkerID (int): Map marker ID
 *
 * Returns map marker data of removed marker or [] on error
 * Map marker data: [target, text, color, regular_icon_path, selected_icon_path, iconClass]
 *
 */

params ["_id"];

private _data = [];
private _idx = Map_Items findIf { _x select 0 == _id };
if ( _idx != -1 ) then {
	_data = Map_Items deleteAt _idx;
	_data deleteAt 0;
};

_data;