/* Filename: 	fn__mapCtrlOnMouseZChanged.sqf
 * Author: 		yuvalino
 * Description: On RscDisplayMap's MapControl MouseZChanged event handler
 */
params ["_map", "_change"];

ctrlMapAnimCommit _map;

true;