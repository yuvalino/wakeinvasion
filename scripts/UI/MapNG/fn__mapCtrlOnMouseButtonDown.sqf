/* Filename: 	fn__mapCtrlOnMouseButtonDown.sqf
 * Author: 		yuvalino
 * Description: On RscDisplayMap's MapControl MouseButtonDown event handler
 */
params ["_ctrlMap", "_btn", "_mousePositionX", "_mousePositionY"];

if ( _btn == 0 ) then 
{
	// Determine selection
	if ( Map_HoverItem == -1 || Map_SelectedItem == Map_HoverItem ) then 
	{
		if ( Map_HoverItem == -1 ) then 
		{
			if ( Map_WaypointItemID != -1 ) then 
			{
				[-1] call yvl_fnc_selectMapMarker;
				[Map_WaypointItemID] call yvl_fnc_removeMapMarker;
				Map_WaypointItemID = -1;
			}
			else
			{
				private _mousePosition = _ctrlMap ctrlMapScreenToWorld [_mousePositionX, _mousePositionY];
				_mousePosition set [2, 0];
				Map_WaypointItemID = [_mousePosition, Map_Waypoint_Text, Map_Waypoint_Color, Map_Waypoint_IconClass] call yvl_fnc_addMapMarker;
				[Map_WaypointItemID] call yvl_fnc_selectMapMarker;
			};
		
		}
		else
		{
			[-1] call yvl_fnc_selectMapMarker;
			
			if ( Map_WaypointItemID != -1 ) then 
			{
				[Map_WaypointItemID] call yvl_fnc_removeMapMarker;
				Map_WaypointItemID = -1;
			};
		};
	}
	else
	{
		[Map_HoverItem] call yvl_fnc_selectMapMarker;
		
		if ( Map_WaypointItemID != -1 ) then 
		{
			[Map_WaypointItemID] call yvl_fnc_removeMapMarker;
			Map_WaypointItemID = -1;
		};
		
	};
};

true;