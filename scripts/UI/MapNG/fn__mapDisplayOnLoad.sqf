/* Filename: 	fn__mapDisplayOnLoad.sqf
 * Author: 		yuvalino
 * Description: On RscDisplayMap Load event handler
 */
params ["_display"];

private _id = -1;

_id = _display displayAddEventHandler ["KeyDown", yvl_fnc__mapDisplayOnKeyDown];
_display setVariable ["eh_onKeyDown", _id];

private _ctrlMap = _display displayCtrl 1000;

_id = _ctrlMap ctrlAddEventHandler ["Draw", yvl_fnc__mapCtrlOnDraw];
_ctrlMap setVariable ["eh_onDraw", _id];
_id = _ctrlMap ctrlAddEventHandler ["MouseMoving", yvl_fnc__mapCtrlOnMouseMoving];
_ctrlMap setVariable ["eh_onMouseMoving", _id];
_id = _ctrlMap ctrlAddEventHandler ["MouseButtonDown", yvl_fnc__mapCtrlOnMouseButtonDown];
_ctrlMap setVariable ["eh_onMouseButtonDown", _id];
_id = _ctrlMap ctrlAddEventHandler ["MouseZChanged", yvl_fnc__mapCtrlOnMouseZChanged];
_ctrlMap setVariable ["eh_onMouseZChanged", _id];

_ctrlMap ctrlMapAnimAdd [0, 0.3, getPos player];
ctrlMapAnimCommit _ctrlMap;
