/* Filename: 	_initRender3D.sqf
 * Author: 		yuvalino
 * Description: Render3D module initialization script
 */

if ( !hasInterface ) exitWith {};
 
intrn__r3d_nxtidx = 0;
intrn__r3d_size = 1 / (getResolution select 5);
intrn__r3d_alpha_mul = 2;
intrn__r3d_alpha_min = 0.05;

Render3D_Items = [];

[
	"Render3d_OnEachFrame",
	"onEachFrame",
	yvl_fnc__draw3DMarkers
] call BIS_fnc_addStackedEventHandler;
