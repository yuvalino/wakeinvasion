/* Filename: 	fn_remove3DMarker.sqf
 * Author: 		yuvalino
 * Description: Removes a 3d marker
 *
 *
 * [3dMarkerID] call yvl_fnc_remove3DMarker
 *
 * 0 - 3dMarkerID (int): 3D marker ID
 *
 * Returns 3d marker data of removed marker or [] on error
 * 3d marker data: [target, color, render3d_icon_path, iconClass]
 *
 */

params ["_id"];

private _data = [];
private _idx = Render3D_Items findIf { _x select 0 == _id };
if ( _idx != -1 ) then {
	_data = Render3D_Items deleteAt _idx;
	_data deleteAt 0;
};

_data;