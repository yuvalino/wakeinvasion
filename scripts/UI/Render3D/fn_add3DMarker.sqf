/* Filename: 	fn_add3DMarker.sqf
 * Author: 		yuvalino
 * Description: Add a new 3D marker
 *
 *
 * [target, color, iconClass] call yvl_fnc_add3DMarker
 *
 * 0 - target (position / object): 	3D marker position / object target (will follow moving objects)
 * 1 - color (array): 				Array of [R,G,B,A] colors (0 to 1 range)
 * 2 - iconClass (string): 			Icon class CfgIcons
 *
 * Returns integer 3d marker ID or -1 on error
 *
 */

params ["_target", "_color", "_iconClass"];

// Get iconClass config data
private _cfg = (missionConfigFile >> "CfgIcons" >> _iconClass);
if (
	!(isClass _cfg) || 
	!(isText (_cfg >> "render3d"))
) exitWith { -1 };

private _icon_render3d = (_cfg >> "render3d") call BIS_fnc_getCfgData;

// Acquire new item ID
private _id = intrn__r3d_nxtidx;
intrn__r3d_nxtidx = intrn__r3d_nxtidx + 1;

// Add new item to the Render3D item list
Render3D_Items pushback [
	_id,
	_target,
	_color,
	format [ "%1%2", MISSION_ROOT, _icon_render3d ],
	_iconClass
];

// Return the newly added item's ID
_id;