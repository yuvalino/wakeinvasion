/* Filename: 	fn__draw3DMarkers.sqf
 * Author: 		yuvalino
 * Description: onEachFrame event handler for drawing 3D markers
 */

{
	_x params ["_id", "_target", "_color", "_icon"];
	
	if (!(_target isEqualType [])) then {
		_target = _target modelToWorldVisual [0,0,0];
	};
	
	private _screenTarget = (worldToScreen _target);
	
	if ((! isNil {_screenTarget}) && (count _screenTarget > 0)) then
	{	
		private _centerDistance = _screenTarget distance [0.5, 0.5];
		_color set [3, intrn__r3d_alpha_min + (intrn__r3d_alpha_mul * _centerDistance)];
	};
	
	private _targetDistance = (player distance _target);

	drawIcon3D
	[
		_icon,
		_color,
		_target,
		intrn__r3d_size,
		intrn__r3d_size,
		0,
		format[ "%1m", round _targetDistance ],
		2,
		0.025,
		"TahomaB",
		"center",
		true
	];
	
} foreach ( Render3D_Items );