
// Execute OnPlayerSpawn scripts
if (isMultiplayer) then {
	waitUntil
	{
		uisleep 0.5;
		(missionNamespace getVariable ["BIS_RscRespawnControlsMap_shown", false]) isEqualTo true
	};
	
	waitUntil
	{
		uisleep 0.5; 
		([
			missionNamespace getVariable ["BIS_RscRespawnControlsMap_shown", false],
			missionNamespace getVariable ["BIS_RscRespawnControlsSpectate_shown", false]
		]) isEqualTo [false, false]
	};
	
	[] remoteExec ["yvl_fnc_onPlayerSpawnServer", 2];
	[] spawn yvl_fnc_onPlayerSpawnClient;
}
else
{
	waitUntil 
	{
		uisleep 0.5;
		alive player
	};
	
	[] remoteExec ["yvl_fnc_onPlayerSpawnServer", 2];
	[] spawn yvl_fnc_onPlayerSpawnClient;
};