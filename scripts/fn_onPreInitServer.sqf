ClientIDs = [];

[east, "L_CSAT_SpecFor_Rifleman"] call BIS_fnc_addRespawnInventory;
[east, getMarkerPos "spawn_northshore", "North Shore"] call BIS_fnc_addRespawnPosition;

addMissionEventHandler ["PlayerDisconnected", 
{
	params ["_id", "_uid", "_name", "_didJIP", "_clientId"];
	
	_clientIdArrIdx = ClientIDs find _clientId;
	
	if (_clientIdArrIdx != -1) then
	{
		ClientIDs deleteAt _clientIdArrIdx;
	};
}];