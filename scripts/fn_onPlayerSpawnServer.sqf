private _clientId = remoteExecutedOwner;
private _clientCount = count (call BIS_fnc_listPlayers);

if !(_clientId in ClientIDs) then {
	ClientIDs pushback _clientId;
};

// If all players are ready
if ((count ClientIDs) == _clientCount) then {
	"All players are ready! Mission starting.." remoteExec ["systemChat", ClientIDs];
	sleep 3;
	call yvl_fnc_onMissionStartServer;
}
else // If not all players are ready
{
	([(_clientCount - (count ClientIDs)), " more players must spawn before mission starts."] joinString "") remoteExec ["systemChat", ClientIDs];
};