/* Filename: 	fn_taskPowerfarmClient.sqf
 * Author: 		yuvalino
 * Description: Main logic for Powerfarm task (client side)
 *
 *
 * [stage, parameters] call yvl_fnc_taskPowerfarmClient
 * 
 * 0 - stage (string): 		Task stage
 * 1 - parameters (array): 	Array of parameters (depends on stage)
 *
 * No return
 *
 */
params ["_stage", "_prm"];

if (!hasInterface) exitWith { };

/* [object_to_attach, completion_time, complete_code, icon_path] call _fn_add_action */
_fn_add_action = {

	params ["_object", "_text", "_complete_time", "_on_complete", "_icon"];

	[
		_object,							// Object the action is attached to
		_text,							// Title of the action
		_icon,								// Idle icon shown on screen
		_icon,								// Progress icon shown on screen
		"_this distance _target < 3",		// Condition for the action to be shown
		"_caller distance _target < 3",		// Condition for the action to progress
		{},									// Code executed when action starts
		{},									// Code executed on every progress tick
		_on_complete,						// Code executed on completion
		{},									// Code executed on interrupted
		[],									// Arguments passed to the scripts as _this select 3
		_complete_time,						// Action duration [s]
		0,									// Priority
		true,								// Remove on completion
		false								// Show in unconscious state 
	] call BIS_fnc_holdActionAdd;

};

switch (_stage) do {

	case "INIT":
	{
		["CustomNotifWhite", ["\a3\ui_f\data\IGUI\Cfg\holdactions\holdAction_unbind_ca.paa", "SABOTAGE", "Sabotage Generators"]] call BIS_fnc_showNotification;
		
		MMARKID_GEN1 = [getPos powerfarm_generator_1, "Sabotage", [1, 0.55, 0, 1], "NoGenerator"] call yvl_fnc_addMapMarker;
		MMARKID_GEN2 = [getPos powerfarm_generator_2, "Sabotage", [1, 0.55, 0, 1], "NoGenerator"] call yvl_fnc_addMapMarker;
		MMARKID_GEN3 = [getPos powerfarm_generator_3, "Sabotage", [1, 0.55, 0, 1], "NoGenerator"] call yvl_fnc_addMapMarker;
		[MMARKID_GEN1] call yvl_fnc_selectMapMarker;
		
		// Add action to generators
		ACTID_PF_GEN_1 = [powerfarm_generator_1, "Sabotage", 12, {
			[_this select 0,_this select 2] call BIS_fnc_holdActionRemove;
			["SABOTAGE", [_this select 0]] remoteExec ["yvl_fnc_taskPowerfarmServer", 2];
		}, "\a3\ui_f\data\IGUI\Cfg\holdactions\holdAction_unbind_ca.paa"] call _fn_add_action;

		ACTID_PF_GEN_2 = [powerfarm_generator_2, "Sabotage", 12, {
			[_this select 0,_this select 2] call BIS_fnc_holdActionRemove;
			["SABOTAGE", [_this select 0]] remoteExec ["yvl_fnc_taskPowerfarmServer", 2];
		}, "\a3\ui_f\data\IGUI\Cfg\holdactions\holdAction_unbind_ca.paa"] call _fn_add_action;

		ACTID_PF_GEN_3 = [powerfarm_generator_3, "Sabotage", 12, {
			[_this select 0,_this select 2] call BIS_fnc_holdActionRemove;
			["SABOTAGE", [_this select 0]] remoteExec ["yvl_fnc_taskPowerfarmServer", 2];
		}, "\a3\ui_f\data\IGUI\Cfg\holdactions\holdAction_unbind_ca.paa"] call _fn_add_action;
	};
	case "SABOTAGE_COMPLETE":
	{
		_prm params ["_object"];
		
		switch (_object) do {
			case powerfarm_generator_1: { [powerfarm_generator_1, ACTID_PF_GEN_1] call BIS_fnc_holdActionRemove };
			case powerfarm_generator_2: { [powerfarm_generator_2, ACTID_PF_GEN_2] call BIS_fnc_holdActionRemove };
			case powerfarm_generator_3: { [powerfarm_generator_3, ACTID_PF_GEN_3] call BIS_fnc_holdActionRemove };
		};
	};
	case "SABOTAGE_STAGE_COMPLETE":
	{
		["TaskSucceeded",["Sabotage Generators"]] call bis_fnc_showNotification;
		ACTID_PF_GEN_1 = nil;
		ACTID_PF_GEN_2 = nil;
		ACTID_PF_GEN_3 = nil;
	};
	case "LAPTOP_STAGE_BEGIN":
	{
		["CustomNotifWhite", ["\a3\ui_f\data\IGUI\Cfg\holdactions\holdAction_hack_ca.paa", "HACK", "Hack Laptop"]] call BIS_fnc_showNotification;
		
		ACTID_PF_LAPTOP = [powerfarm_laptop, "Hack", 12, {
			[_this select 0,_this select 2] call BIS_fnc_holdActionRemove;
			["HACK"] remoteExec ["yvl_fnc_taskPowerfarmServer", 2];
		}, "\a3\ui_f\data\IGUI\Cfg\holdactions\holdAction_hack_ca.paa"] call _fn_add_action;
	};
	case "LAPTOP_STAGE_COMPLETE":
	{
		["TaskSucceeded",["Hack laptop"]] call bis_fnc_showNotification;
		[powerfarm_laptop, ACTID_PF_LAPTOP] call BIS_fnc_holdActionRemove;
		ACTID_PF_LAPTOP = nil;
	};

};





