/* Filename: 	fn_taskPowerfarmServer.sqf
 * Author: 		yuvalino
 * Description: Main logic for Powerfarm task (server side)
 *
 *
 * [stage, parameters] call yvl_fnc_taskPowerfarmServer
 * 
 * 0 - stage (string): 		Task stage
 * 1 - parameters (array): 	Array of parameters (depends on stage)
 *
 * No return
 *
 */
params ["_stage", "_prm"];

if ( isServer ) then {
	switch (_stage) do {
		case "INIT":
		{
			SabotagedGenerators = [];
			["INIT"] remoteExec ["yvl_fnc_taskPowerfarmClient"];
		};
		case "SABOTAGE":
		{
			if ( isNil "SabotagedGenerators" ) exitWith { };
			
			_prm params ["_object"];
		
			if (!(_object in SabotagedGenerators)) then {
				SabotagedGenerators pushback _object;
			};
			
			["SABOTAGE_COMPLETE", [_object]] remoteExec ["yvl_fnc_taskPowerfarmClient"];
			
			if (powerfarm_generator_1 in SabotagedGenerators &&
				powerfarm_generator_2 in SabotagedGenerators &&
				powerfarm_generator_3 in SabotagedGenerators) then {
				["SABOTAGE_STAGE_COMPLETE"] call yvl_fnc_taskPowerfarmServer;
			};
		};
		case "SABOTAGE_STAGE_COMPLETE":
		{
			SabotagedGenerators = nil;
			["SABOTAGE_STAGE_COMPLETE"] remoteExec ["yvl_fnc_taskPowerfarmClient"];
			["LAPTOP_STAGE_BEGIN"] call yvl_fnc_taskPowerfarmServer;
		};
		case "LAPTOP_STAGE_BEGIN":
		{
			["LAPTOP_STAGE_BEGIN"] remoteExec ["yvl_fnc_taskPowerfarmClient"];
		};
		
		case "HACK":
		{
			["LAPTOP_STAGE_COMPLETE"] remoteExec ["yvl_fnc_taskPowerfarmClient"];
		};
	};
};