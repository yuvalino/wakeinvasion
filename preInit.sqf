MISSION_ROOT = call {
    private "_arr";
    _arr = toArray str missionConfigFile;
    _arr resize (count _arr - 15);
    toString _arr
};

if (isServer) then {
	[] spawn yvl_fnc_onPreInitServer;
};

if (hasInterface) then {
	[] spawn yvl_fnc_onPreInitClient;
};