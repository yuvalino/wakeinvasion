if (isServer) then {
	[] spawn yvl_fnc_onPostInitServer;
};

if (hasInterface) then {
	[] spawn yvl_fnc_onPostInitClient;
};